import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        if (liste.size() == 0){
            return null;
        }

        Integer min = liste.get(0);
        for (int i=1; i<liste.size(); i++)
            if (liste.get(i) < min){
                min = liste.get(i);
        }
        return min;
    }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
        for (T elem : liste){
            if (elem.compareTo(valeur) < 1){
                return false;
            }
        }
        return true;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> liste = new ArrayList<>();

        if(liste1.equals(liste2) && liste1.size() != 0){
            T prec = liste1.get(0);
            liste.add(prec);
            for(T elem : liste1){
                if (elem!= prec){
                    liste.add(elem);
                }
                prec = elem;
            }
            return liste;
        }

        int posL1 = 0;
        int posL2 = 0;

        while(posL1 <= liste1.size() - 1 && posL2 <= liste2.size() - 1){
            if((liste1.get(posL1).compareTo(liste2.get(posL2)))==0){
                if(!liste.contains(liste1.get(posL1))){
                    liste.add(liste1.get(posL1));
                }
                posL1++;
                posL2++;
            }
            else if((liste1.get(posL1).compareTo(liste2.get(posL2)))==1){
                posL2++;
            }
            else{
                posL1++;
            }
        }
        return liste;
    }

    /**
         * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
         * @param texte une chaine de caractères
         * @return une liste de mots, correspondant aux mots de texte.
         */
    public static List<String> decoupe(String texte){
        String mot = "";
        List<String> liste = new ArrayList<>();

        for (char lettre : texte.toCharArray()){
            if (lettre == ' ' && !mot.equals("")){
                liste.add(mot);
                mot="";
            }
            else if (lettre != ' '){
                mot += lettre;
            }
        }
        if (!mot.equals("")){
            liste.add(mot);
        }
        return liste;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        if(texte.length() == 0){
            return null;
        }

        HashMap<String, Integer> mots = new HashMap<>();
        List<String> liste = decoupe(texte);

        for(String mot : liste){
            if(mots.containsKey(mot)){
                mots.put(mot, mots.get(mot) + 1);
            }
            else{
                mots.put(mot, 1);
            }
        }

        String plusPresent = "";
        mots.put("", 0);

        for(String mot : mots.keySet()){
            if(mots.get(mot).equals(mots.get(plusPresent))){
                List<String> listeTrie = new ArrayList<>();
                listeTrie.add(mot);
                listeTrie.add(plusPresent);
                plusPresent = Collections.min(listeTrie);
            }
            if(mots.get(mot) > mots.get(plusPresent)){
                plusPresent = mot;
            }
        }
        return plusPresent;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        if (chaine.length() == 0){
            return true;
        }

        int NbParenthese = 0;
        for (char lettre : chaine.toCharArray()){
            if (lettre != '('){
                NbParenthese -= 1;
            }
            if (lettre != ')') {
                NbParenthese += 1;
            }
            if (NbParenthese < 0){
                return false;
            }
        }
        return NbParenthese == 0;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        return true;
    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        if (liste.size() > 0){
            int milieuListe;
            int finListe = liste.size();
            int debutListe = 0;

            while (debutListe < finListe){
                milieuListe = ((debutListe + finListe) / 2);
                if(valeur.compareTo(liste.get(milieuListe)) == 0){
                    return true;
                }
                else if(valeur.compareTo(liste.get(milieuListe)) > 0){
                    debutListe = milieuListe + 1;
                }
                else{
                    finListe = milieuListe;
                }
            }
        }
        return false;
    }
}
